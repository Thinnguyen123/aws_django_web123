from django.contrib import admin
from django.urls import path
from products import views
from django.conf.urls.static import static
from django.conf import settings


urlpatterns =  static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) + [
    path('', views.addProduct, name="add-prod"),  
    path('edit-product/<str:pk>', views.editProduct, name="edit-prod"),
]
#if settings.DEBUG:
 #   urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
 #   urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

